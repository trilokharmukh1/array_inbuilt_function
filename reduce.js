//--- this function is work like inbuilt reduce function

function reduce(elements, cb, startingValue) {
    let acc;

    if (Array.isArray(elements) === false) {
        return "invalid input";
    }
    
    if (elements.length === 0 && startingValue===undefined) { 
        return undefined;
    }

    //if starting value is not passed accumulator initialize to array[0] and current is initialize  with array[1] 
    if (startingValue === undefined) {
        acc = elements[0];
        for (let index = 1; index < elements.length; index++) {
            acc = cb(acc, elements[index], index, elements);
        }
    }
    else {
        acc = startingValue;
        for (let index = 0; index < elements.length; index++) {
            acc = cb(acc, elements[index], index, elements);
        }

    }

    return acc;
}

module.exports = reduce;