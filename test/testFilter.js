//-- test for filter function

const filter = require('./../filter');
const shaps =  ['triangle', 'square', 'triangle', 'circle', 'triangle'];

const result = filter(shaps, function (element, index, shaps) {
  console.log('original array ', shaps); // it should log ['triangle', 'square', 'triangle', 'circle', 'triangle']
  return (element === 'triangle');
});

console.log(result);