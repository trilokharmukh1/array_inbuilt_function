//-------this function work like forEach function

function each(element, cb){
    // checking element is array or not
    if(Array.isArray(element)!==true){
        return "function only work on array";
    }

    for(let x=0; x<element.length; x++){
       cb(element[x])
       //callling call back function and passing array element
    }


}

module.exports=each;