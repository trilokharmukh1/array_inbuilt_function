//-- this function work like a inbuilt flat function

function flatten(element, depths) {
    let depth = depths;
    let ansArray = [];
    
    if(Array.isArray(element)===false){
        return [];
    }

    if (depth === undefined) {
        depth = 1;
    }

    for (let index = 0; index < element.length; index++) {

        (Array.isArray(element[index]) === true && depth > 0)        
        ? ansArray = ansArray.concat(flatten(element[index], depth - 1))   //if array found recursion called
        : (element[index]!==undefined)             // push only if element is not a undefined value
            ? ansArray.push(element[index])
            : ""

    }
    return ansArray;

}

module.exports = flatten;