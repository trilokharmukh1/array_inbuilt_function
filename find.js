// this function work link inbuilt find function

function find(element, cb) {

    // checking passing element is array or not
    if (Array.isArray(element) !== true) {
        return "first argument must be array";
    }

    for (let index = 0; index < element.length; index++) {
        let ele = cb(element[index]);           // if data find return true
        if (ele === true) {
            return element[index];              // return first matched element
        }
    }

}

module.exports = find;
