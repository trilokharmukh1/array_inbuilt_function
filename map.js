//---this function is a recreate of .map in inbuilt function

function map(element, cb) {

    // checking passing data is array or not
    if (Array.isArray(element) !== true) {
        return "first argument must be array";
    }

    let newArray = [];

    for (let index = 0; index < element.length; index++) {
        let result = cb(element[index], index, element);        // calling call back function and store return value
        
        if(result!=="undefined"){
            newArray.push(result);               //push data in array 
        }
        
    }
    return newArray;
}

module.exports = map;