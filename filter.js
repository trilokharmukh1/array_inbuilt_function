// this function is a receate of filter function

function filter(element, cb){

    //checking element is array or not
    if(Array.isArray(element)===false){
        return "first argument must be array"
    }

    let result = [];
    for(let index=0; index<element.length; index++){
        let checkData = cb(element[index], index, element);    //searching matched data  
        if(checkData===true){
            result.push(element[index]);       // push in array if matched 
        }
    }
    return result;
}

module.exports = filter;